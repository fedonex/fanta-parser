from bs4 import BeautifulSoup
from model.fantaplayer import FantaPlayer
from model.player import Player
import json
import os
import urllib2

"""
    A simple parser for scraping votes from gazzetta.it with BeautifulSoup
    Author: fedone
    GitHub: @fedone
"""


class FantaParser:
    def __init__(self, day):
        self.mainpage_url = 'https://www.gazzetta.it/calcio/fantanews/voti/serie-a-2019-20/giornata-' + day
        self.player_list = []
        fp = os.getcwd() + '\\input\\' + day + '.json'
        fp_heroku = os.getcwd() + '/input/' + day + '.json'
        try:
            f = open(fp)
            k = f.read()
            self.league = json.loads(k)
        except IOError:
            f = open(fp_heroku)
            k = f.read()
            self.league = json.loads(k)
        except ValueError:
            print '-------------- Invalid JSON format --------------'
        finally:
            f.close()

    def get_main_html(self):
        main_html = urllib2.urlopen(self.mainpage_url)
        html = main_html.read()
        return html

    def add_points_for_captain(self, plist):
        player_list = sorted(plist, key=lambda i: i['points'], reverse=True)
        try:
            player_list[0]['fp'].increment_points(3)
            print '---------------- Captain sorting ----------------'
            if player_list[0]['points'] > player_list[1]['points']:
                print player_list[0]['fp'].name + ' wins the captain bonus with ' + player_list[0]['name'] + '!'
                print 'Final points = ' + str(player_list[0]['fp'].point)
            else:
                print 'Pair result, no one get the bonus points'
        except IndexError:
            '------------ Nessun giocatore presente ------------'

    def parse_for_players(self):
        html = self.get_main_html()
        try:
            fake_player_list = self.league['league']

            soup = BeautifulSoup(html, 'html.parser')
            player_list = soup.find_all('ul', {'class': 'magicTeamList'})
            captains_points = []
            for fanta_player in fake_player_list:
                try:
                    fp = FantaPlayer(fanta_player['name'])
                    print '-------------------------------------------'
                    print '------ Risultati ' + fanta_player['name'] + ' ---------------------'
                    for name in fanta_player['players']:
                        for row in player_list:
                            if row.find('li', {'class': 'head'}).find('div', {'class': 'teamName'}).find('span', {'class': 'teamNameIn'}).text == name['team']:
                                for singlePlayer in row.find_all('li', {'class': None}):
                                    player_name = None
                                    if singlePlayer.find('a') is not None:
                                        player_name = singlePlayer.find('a').text
                                    if name['name'] == player_name:
                                        vote = singlePlayer.find('div', {'class': 'inParameter vParameter'}).text
                                        FantaVote = singlePlayer.find('div', {'class': 'inParameter fvParameter'}).text
                                        print '-------------------------------------------'
                                        if name['isCaptain'] is True:
                                            print 'Nome: ' + name['name'] + '(C.)'
                                        else:
                                            print 'Nome: ' + name['name']

                                        print 'Voto: ' + vote
                                        captains_points.append({'fp': fp, 'points': vote, 'name': name['name']})
                                        print 'Fantavoto: ' + FantaVote
                                        fp.increment_points(FantaVote)

                    print '-------------- totale ---------------------'
                    pl = fp.get_result()
                    print 'Giocatori schierati: ' + str(pl['valid_players']) + '/11'
                    print 'Punteggio Totale: ' + str(pl['points'])

                except AttributeError:
                    print '----------- This matchday its not available yet, try later -----------'

            self.add_points_for_captain(captains_points)
        except AttributeError:
            print '------------- Input JSON missing  ---------------'
