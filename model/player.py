class Player:

    def __init__(self, name, vote, owner):
        self.name = name
        self.vote = vote
        self.owner = owner

    def return_model(self):
        player = {
            'name': self.name,
            'vote': self.vote
        }
        return player
