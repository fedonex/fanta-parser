class FantaPlayer:
    def __init__(self, name):
        self.point = 0
        self.name = name
        self.valid_players = 0

    def increment_points(self, point):
        self.valid_players = self.valid_players + 1
        if point != '-':
            try:
                int(point)
            except ValueError:
                integer = float(point.split('.')[0]) * 2
                self.point = self.point + ((integer + 1) / 2)
            else:
                self.point = self.point + int(point)
        else:
            self.point = self.point + 0

    def get_result(self):
        return {
            'name': self.name,
            'points': self.point,
            'valid_players': self.valid_players
        }



