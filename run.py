from lib.parser import FantaParser

if __name__ == "__main__":

    print """
    ###########################################
    # Secret Fantatool for lazy people v0.1.3 #
    ###########################################
    """

    day = raw_input('For witch matchday you want to get the stats? ')
    try:
        if day > 0 and type(int(day)) is int:
                print 'Getting stats from ' + 'matchday n. ' + day + '...'
                fp = FantaParser(day)
                fp.parse_for_players()
        else:
            print 'Please specify a valid matchday you want to open'
    except ValueError:
        'Please specify a valid matchday you want to open'
